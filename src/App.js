import React from 'react';
import './App.css';
import Welcome from './components/Welcome';
import { Route } from 'react-router-dom';
import HomePage from './components/HomePage';

function App() {
  return (
    <div className="App">
     
       <Route exact path="/" component={Welcome}/>
       <Route path="/home" component={HomePage}/>

    </div>
  );
}

export default App;
