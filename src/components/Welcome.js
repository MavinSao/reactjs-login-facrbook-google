import React, { Component } from 'react'
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import {Redirect} from 'react-router-dom';
import './../App.css';

export default class Welcome extends Component {

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            res: null,
            type: null,
        }
    }

    render() {

        if(this.state.redirect){
            return(<Redirect to={{
                pathname: '/home',
                state: this.state
            }}/>)
        }

        const responseFacebook = (response) => {
            this.setState({
                redirect: true,
                res: response,
                type: 'facebook'
            })
            
            
        }
        const responseGoogle = (response) => {
            this.setState({
                redirect: true,
                res: response,
                type: 'google'
            })  
        }

        return (
            <div className="App">
                <h1>Login Page</h1>
                <GoogleLogin
                    clientId="325259345914-0501c2809ng293itmfo3brhv6uj317e3.apps.googleusercontent.com"
                    buttonText="Login"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                    cookiePolicy={'single_host_origin'}
                /> <br/><br/>
                <FacebookLogin
                    appId="232259517952785"
                    fields="name,email,picture"
                    callback={responseFacebook} 
                />
            </div>
        )
    }
}
