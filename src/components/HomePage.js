import React, { Component } from 'react'
import './../App.css';
import { Redirect } from 'react-router';
export default class HomePage extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLogout: false,
            profilePath: '',
            userName: '',
            email: ''
        }
    }
    componentWillMount(){
        let data = this.props.location.state;
        console.log(data);
        
        if(data.type === 'google'){  
            this.setState({
                profilePath: data.res.Qt.UK,
                userName: data.res.Qt.Ad,
                email: data.res.Qt.zu
            })
        }else{
            this.setState({
                profilePath: data.res.picture.data.url, 
                userName:  data.res.name,
                email: data.res.email
            })
        }
    }
    render() {
        if (this.state.isLogout){
            return <Redirect to="/"/>
        }
        return (
            <div>
                <h1>Your Profile</h1>
                {console.log(this.state)}
                <img src={this.state.profilePath}/>
                <h2>{this.state.userName}</h2>
                <h2>{this.state.email}</h2>
                <button onClick={()=>{
                    this.setState({
                        isLogout: true
                    })
                }}>Logout</button>
            </div>
        )
    }
}
